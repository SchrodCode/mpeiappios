//
//  NewsParser.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 04.08.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import Foundation
import Alamofire

class NewsParser{
    //MARK: Singleton
    static let instance = NewsParser()
    private init(){
        parseNews()
    }
    //MARK: Variables
    fileprivate var newsList: [NewsData] = []
    fileprivate var observerCollection = NSMutableSet()
    fileprivate var currentPage: Int = 0
    fileprivate var isParsing = false
    
    //MARK: Getters
    
    var newsCount: Int {
        get {
            return newsList.count
        }
    }
    var connection: Bool = true
    
    var cellsCount: Int {
        get {
            if !connection {
                return 1
            }
            return newsCount
        }
    }
    
    var isEmpty: Bool {
        get {
            return newsCount > 0
        }
    }
    
    func getData(atIndex: Int) -> NewsData {
        return newsList[atIndex]
    }
    
    //MARK: News
    func refreshNews(){
        self.currentPage = 0
        self.newsList = []
        parseNews()
    }
    
    func getType(at indexPath: IndexPath)-> NewsCellType{
        if(indexPath.row % 10 == 8){
            return .Advert
        }
        if(!connection){
            return .EmptyPlaceholder
        }
        return .News
    }
    
    

    
    
    func parseNews(){
        if !isParsing {
            isParsing = true
            self.currentPage += 1
            Alamofire.request("http://mpei.ru/news/Pages/news.aspx?p=\(self.currentPage)").responseData { responseData in
            switch responseData.result {
            case .success(let value):
                self.connection = true
                guard let string = NSString(data: value, encoding: String.Encoding.utf8.rawValue) else { return }
                var urlContentArray = string.components(separatedBy: "<div class=\"item newsitem")
                urlContentArray.removeFirst()
                for item in urlContentArray {
                    
                    // Получили URL картинки
                    let address =  item.components(separatedBy: " src=\"")[1]
                    let imageUrl = "http://mpei.ru" + address.components(separatedBy:"\" /><div cla")[0]
                    
                    //Текст новости
                    var text = item.components(separatedBy: "title=\"\" style=\"\">")[1]
                    text = text.components(separatedBy: "</a></div>")[0]
                    
                    //Дата
                    var date =  item.components(separatedBy: "newsdate\">")[1]
                    date = date.components(separatedBy:"</div>")[0]
                    
                    //Ссылка на новость
                    var newsUrl =  item.components(separatedBy: "<a href=\"")[1]
                    newsUrl = newsUrl.replacingOccurrences(of: "amp;", with: "")
                    newsUrl = "http://mpei.ru" + newsUrl.components(separatedBy:"\" title")[0]
                    let news = (NewsWebsiteData(text: text, date: date, newsUrl : newsUrl))
                    self.newsList.append(news)
                    self.notifyObjects()
                }
                
                case .failure(let value):
                    self.connection = false
                    print(value)
            }
            
            self.isParsing = false
            }
        }
    }
}


// MARK: NewsSubject
extension NewsParser: NewsSubject {
    func notifyObjects() {
        for observer in observerCollection {
            (observer as! NewsObserver).newsDataChanged()
        }
    }
    
    func addObserver(observer: NewsObserver) {
        observerCollection.add(observer)
        self.notifyObjects()
    }
    func removeObserver(observer: NewsObserver) {
        observerCollection.remove(observer)
    }
    
}
//MARK: - Protocols
protocol NewsSubject {
    func addObserver(observer: NewsObserver)
    func removeObserver(observer: NewsObserver)
    func notifyObjects()
}

protocol NewsObserver {
    func newsDataChanged()
}

