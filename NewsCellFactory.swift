//
//  NewsTableCellFactory.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 14.11.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import Foundation
import UIKit

class NewsCellFactory {
    
    class func getCell(at indexPath: IndexPath, vc: NewsViewController) -> NewsTableCell {
        let type = getTypeForCell(at: indexPath)
        let id = getIdentifierForCell(at: indexPath)
        switch type {
            case .News:
                let cell = vc.newsTable.dequeueReusableCell(withIdentifier: id, for: indexPath) as! NewsTableViewCell
                cell.data = NewsParser.instance.getData(atIndex: indexPath.row) as! NewsWebsiteData
                cell.newsDate.text = cell.data?.dateString
                cell.newsText.text = cell.data?.text
                if(indexPath.row % 2 == 0){
                    cell.data?.sourse = FeedSources.Website
                } else {
                    cell.data?.sourse = FeedSources.Twitter
                }
                cell.sourseLabel.text = cell.data?.getSourseName()
                cell.sourceImage.image = cell.data?.getSourceImage()
                return cell
            case .Advert:
                let cell = vc.newsTable.dequeueReusableCell(withIdentifier: id, for: indexPath) as! NewsAdvertViewCell
                cell.adTitleLabel.text = "ADVERTISEMENT"
                cell.backgroundView?.backgroundColor = UIColor.MpeiApp.barTint
                return cell
            case .EmptyPlaceholder:
                return vc.newsTable.dequeueReusableCell(withIdentifier: id, for: indexPath) as! EmptyPlaceholderViewCell
        }
    }
    
    class func getTypeForCell(at indexPath: IndexPath) -> NewsCellType {
        return NewsParser.instance.getType(at: indexPath)
    }
    
    class func getHeightForCell(at indexPath: IndexPath) -> CGFloat {
            let type = NewsParser.instance.getType(at: indexPath)
            switch type {
            case .News:
                return 120.0
            case .Advert:
                return 120.0
            case .EmptyPlaceholder:
                return 320.0
        }
    }
    
    
    class func getIdentifierForCell(at indexPath: IndexPath) -> String {
        let type = NewsParser.instance.getType(at: indexPath)
        switch type {
            case .News:
                return "news"
            case .Advert:
                return "ad"
            case .EmptyPlaceholder:
                return "empty"
            
        }
    }
    
}

