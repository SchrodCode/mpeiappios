//
//  FeedSourse.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 19.11.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import Foundation
import UIKit

class FeedSourse {
    private var name_: String?
    private var image_: UIImage?
    var image: UIImage? {
        get {
            return image_
        }
    }
    
    var name: String? {
        get {
            return name_
        }
    }
    
    init(name: String, image: UIImage){
        self.name_ = name
        self.image_ = image
    }
}

struct FeedSources{
    static var Website = FeedSourse(name: "Сайт МЭИ", image: #imageLiteral(resourceName: "mpei-icon-small-flatGray"))
    static var Twitter = FeedSourse(name: "Твиттер МЭИ", image: #imageLiteral(resourceName: "icons8-twitter_filled"))
}
