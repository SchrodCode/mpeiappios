//
//  NewsData.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 16.11.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import Foundation
import UIKit

enum NewsDataType {
    case WebsiteNews
    case Advert
}

protocol NewsData {
    var type: NewsDataType {get}
    var date: Date? {get set}
    var sourse: FeedSourse?{get set}
    func getSourseName() -> String
    func getSourceImage()-> UIImage
}



