//
//  NewsWebsiteData.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 16.11.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import Foundation
import SwiftyJSON

class NewsWebsiteData: NewsData {
    var sourse: FeedSourse?
    func getSourseName() -> String {
        return (self.sourse?.name)!
    }
    
    func getSourceImage()-> UIImage {
        return (self.sourse?.image)!
    }
    
    
    var type: NewsDataType {
        get {
            return .WebsiteNews
        }
    }
    var text: String?
    var newsUrl: URL?
    var textView = FeedText("abc")
    var date: Date?
    var dateString: String {
        get {
            let dateformatter = DateFormatter()
            dateformatter.dateFormat = "dd.mm.yyyy"
            guard (date != nil) else {
                return ""
            }
            return dateformatter.string(from: date!)
        }
    }
    
    init(text: String, date: String, newsUrl: String){
        self.text = text
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd.mm.yyyy"
        self.date = dateformatter.date(from: date)
        self.newsUrl = URL(string: newsUrl)
    }
    
    init(json: JSON){
        self.text = json["text"].string ?? ""
        let date = json["date"].int ?? 0
        self.date = Date(timeIntervalSince1970: TimeInterval(date))
        let url = json["url"].string ?? "www.mpei.ru"
        self.newsUrl = URL(string: url)
    }
}
