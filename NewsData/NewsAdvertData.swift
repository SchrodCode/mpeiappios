//
//  NewsAdvertData.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 16.11.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import Foundation
import UIKit

class NewsAdvertData: NewsData {
    var sourse: FeedSourse?
    
    func getSourseName() -> String {
        return (sourse!.name)!
    }
    
    func getSourceImage() -> UIImage {
        return (sourse!.image)!
    }
    
    
    var type: NewsDataType {
        get {
            return .Advert
        }
    }
    
    var text: String?
    var date: Date?
    
}
