//
//  Resourse.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 19.11.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import Foundation
import UIKit

enum FeedResourseType {
    case Text
    case Image
    case Link
    case Video
    case Audio
}

protocol FeedResourse {
    var type: FeedResourseType? {get}
}

class FeedText: FeedResourse {
    var type: FeedResourseType? {
        get {
            return .Text
        }
    }
    
    var text: String?
    
    init(_ text: String){
        self.text = text
    }
}



