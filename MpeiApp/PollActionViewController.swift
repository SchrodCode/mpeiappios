//
//  PollActionViewController.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 07.07.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import UIKit
import Spring

class PollActionViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, QuestionObserver {
    // MARK: - Variables
    var poll: PollData?
    
    @IBOutlet weak var navigation: UINavigationItem!
    @IBOutlet weak var answerButton: UIButton!
    @IBOutlet weak var pollQuestionTableView: UITableView!
    @IBOutlet weak var pollQuestionLabel: UILabel!
    @IBOutlet weak var pollDescriptionLabel: SpringLabel!
    
    var currentQuestion: PollQuestion? {
        return poll?.getCurrentQuestionData()
    }
    
    @IBAction func backPressed(_ sender: Any) {
        prevQuestion()
    }
    
    @IBAction func answerPressed(_ sender: Any) {
        if (poll?.isLast())! {
            finishPoll()
        } else {
            nextQuestion()
        }
    }
    
    func updateButton(){
        if (poll?.isLast())! {
            answerButton.setTitle("Завершить", for: .normal)
        } else {
            answerButton.setTitle("Далее", for: .normal)
        }
    }
    
    func nextQuestion(){
        poll?.next()
    }
    
    func prevQuestion(){
        poll?.prev()
    }
    
    func finishPoll(){
        poll?.showAnswers()
        dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Viewdidload

    override func viewDidLoad() {
        super.viewDidLoad()
        poll?.addObserver(observer: self)
    }
    
    //MARK: - Table View
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let number = currentQuestion?.getVariantsCount()
        return number ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PollQuestionTableViewCell", for: indexPath) as! PollQuestionTableViewCell
        cell.questionTextLabel.text = currentQuestion?.getVariantText(atIndex: indexPath.row)
        currentQuestion?.addObserver(observer: cell, atIndex: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let index = indexPath.row
        currentQuestion!.switchSelection(atIndex: index)
    }
    
    // MARK: Observer
    func questionChanged() {
        updateNavigationBar()
        updateView()
        updateButton()
    }

    func updateNavigationBar() {
        let count = (poll?.getQuestionsCount())!
        let numberToShow = (poll?.getCurrentQuestionNumber())! + 1
        self.navigation.title = "Вопрос: \(numberToShow) / \(count)"
    }
    
    func updateView(){
        pollQuestionLabel.text = currentQuestion?.getText()
        if (currentQuestion?.toSelect())! > 1 {
            pollDescriptionLabel.text = "Выберите несколько вариантов ответа"
        } else {
            pollDescriptionLabel.text = "Выберите один вариант ответа"
        }
        pollQuestionTableView.reloadData()
    }

}











