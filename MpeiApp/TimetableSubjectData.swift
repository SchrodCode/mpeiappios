//
//  TimetableCellData.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 08.06.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import Foundation
enum TimetableSubjectType {
    case Lab
    case Lecture
    case Practice
    case Exam
    case Other
    case Consult
}

class TimetableSubjectData {
    var building : String?
    var subject : String?
    var time: String?
    var count: String?
    var type: TimetableSubjectType
    var teacher: String?
    
    init(building: String, subject : String,time: String, count: String, type: TimetableSubjectType, teacher: String){
        self.building = building
        self.subject = subject
        self.time = time
        self.count = count
        self.type = type
        self.teacher = teacher
    }
}
