//
//  DefaultColors.swift
//  MpeiApp
//

import Foundation
import UIKit

//MARK: Colors
extension UIColor {
    struct MpeiApp {
        static var barTint: UIColor  { return #colorLiteral(red: 0, green: 0.4128231108, blue: 0.5337332487, alpha: 1) }
        static var darkRedColor: UIColor { return #colorLiteral(red: 0.8196078431, green: 0.168627451, blue: 0.1254901961, alpha: 1) }
    }
}


class System {
    //MARK: Singleton
    static let instance = System()
    private init(){}
    
    //MARK: Notification
    class func noticeTop(sender: UIViewController, text: String){
        sender.noticeTop(text, autoClear: true, autoClearTime: 1)
    }
    
    class func makeShadow(view: UIView?, width: Int = 0, height: Int = 2, opacity: Double = 0.2){
        view?.clipsToBounds = false
        view?.layer.shadowColor = UIColor.black.cgColor
        view?.layer.shadowOffset = CGSize(width: 0, height: 2)
        view?.layer.shadowOpacity = 0.2;
    }
}
