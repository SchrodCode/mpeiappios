//
//  PollData.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 06.07.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import Foundation
class PollData: QuestionSubject {
    
    //MARK: - Variables

    fileprivate var name: String?
    fileprivate var description: String?
    fileprivate var imageURL: String?
    fileprivate var questions: [PollQuestion]
    fileprivate var author: String?
    fileprivate var date: String?
    fileprivate var currentQuestionNumber: Int
    fileprivate var observerCollection = NSMutableSet()
    fileprivate var isAnswered: Bool
    
    //MARK: - Inits
    init(name: String, description: String, questions: [PollQuestion], author: String, date: String?, isAnswered: Bool = false){
        self.name = name
        self.description = description
        self.questions = questions
        self.author = author
        self.date = date
        self.currentQuestionNumber = 0
        self.isAnswered = isAnswered
    }
    
    //MARK: - Getters
    
    func getQuestionsCount() -> Int {
        return self.questions.count
    }
    
    func getName() -> String?{
        return self.name
    }
    
    func getDescription() -> String?{
        return self.description
    }
    
    func getDate() -> String?{
        return self.date
    }
    
    func getImageURL() -> String?{
        return self.imageURL
    }

    func getAuthor() -> String?{
        return self.author
    }
    
    //MARK: - Current Getters
    
    func getCurrentQuestionData() -> PollQuestion {
        return self.questions[currentQuestionNumber]
    }
    
    func getCurrentQuestionNumber() -> Int {
        return currentQuestionNumber
    }
    
    func getCurrentQuestionText() -> String? {
        return self.questions[currentQuestionNumber].getText()
    }
    func getCurrentQuestionType() -> QuestionType {
        return self.questions[currentQuestionNumber].getType()
    }
    
    
    //MARK: - Iteration
    
    func isLast() -> Bool {
        return currentQuestionNumber == self.questions.count - 1
    }
    
    func isFirst() -> Bool {
        if (self.currentQuestionNumber == 0){
            return true
        }
        return false
    }
    func next(){
        if !isLast(){
            self.currentQuestionNumber += 1
        }
        self.notifyObjects()

    }
    func prev(){
        if !isFirst(){
            self.currentQuestionNumber -= 1
        }
        self.notifyObjects()
    }
    //MARK: - Observer
    
    func notifyObjects() {
        for observer in observerCollection {
            (observer as! QuestionObserver).questionChanged()
        }
    }
    
    func addObserver(observer: QuestionObserver) {
        self.observerCollection.add(observer)
        self.notifyObjects()
    }
    
    func removeObserver(observer: QuestionObserver) {
        self.observerCollection.remove(observer)
    }
    
    func showAnswers(){
        for (i,question) in self.questions.enumerated() {
            print(" \(i):")
            question.show()
        }
    }
    
    func isFinished() -> Bool {
        return self.isAnswered
    }
}

protocol QuestionSubject {
    func addObserver(observer: QuestionObserver)
    func removeObserver(observer: QuestionObserver)
    func notifyObjects()
}

protocol QuestionObserver{
    func questionChanged()
}
