//
//  SubjectViewController.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 15.06.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import UIKit

class SubjectViewController: UIViewController {
    var data: TimetableSubjectData = TimetableSubjectData(building: "A-001",subject: "Предмет",time: "00:00 - 23:59", count: "1", type: TimetableSubjectType.Practice, teacher: "Маран Микхель Микхелевич")
    
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var timeLabel: FancyLabel!
    @IBOutlet weak var buildingLabel: FancyLabel!
    @IBOutlet weak var teacherLabel: FancyLabel!
    @IBOutlet weak var subjectLabel: FancyLabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        teacherLabel.text = data.teacher
        subjectLabel.text = data.subject
        timeLabel.text = data.time
        buildingLabel.text = data.building
        
    }
    
}
