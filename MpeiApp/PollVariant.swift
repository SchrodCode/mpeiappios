//
//  PollVariant.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 09.07.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import Foundation

class PollVariant: VariantSubject{
    fileprivate var text: String
    fileprivate var isSelectedValue: Bool
    fileprivate var observerCollection = NSMutableSet()
    
    init(text: String){
        self.text = text
        self.isSelectedValue = false
    }
    
    func select(){
        self.isSelectedValue = true
        self.notifyObjects()
    }
    
    func deselect(){
        self.isSelectedValue = false
        self.notifyObjects()
        for item in self.observerCollection {
            print(item)
        }
    }
    
    func isSelected() -> Bool {
        return self.isSelectedValue
    }
    
    func getText() -> String{
        return self.text
    }

    //MARK: VariantSubject
    func addObserver(observer: VariantObserver) {
        self.observerCollection.add(observer)
        self.notifyObjects()
    }
    
    func removeObserver(observer: VariantObserver) {
        self.observerCollection.remove(observer)
    }
    
    func notifyObjects() {
        for observer in observerCollection {
            (observer as! VariantObserver).statusChanged(checked: self.isSelectedValue)
        }
    }
}

protocol VariantSubject {
    func addObserver(observer: VariantObserver)
    func removeObserver(observer: VariantObserver)
    func notifyObjects()
}

protocol VariantObserver{
    func statusChanged(checked: Bool)
}

