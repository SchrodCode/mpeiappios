//
//  ProgressViewController.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 27.06.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import UIKit
import Spring
import MessageUI

class PollTableViewController: UITableViewController,MFMailComposeViewControllerDelegate {
   
    // MARK: - Variables

    var polls : [PollData] = [
        PollData(name: "История МЭИ", description: "Опрос от деканата, все ответы записываются", questions: [
            PollQuestion(text: "Насколько вы любите МЭИ?", variants:
                [PollVariant(text: "5"),
                 PollVariant(text: "4"),
                 PollVariant(text: "2"),
                 PollVariant(text: "10/10")])
            ], author: "НИУ МЭИ", date: "14.06.2017"),
        PollData(name: "5 столовая", description: "Мы заметили, что популярность нашей столовой среди студентов несколько упала, в связи с чем хотим узнать, что нужно улучшить ради наших клиентов.", questions: [
            PollQuestion(text: "Устраивает ли вас ассортимент блюд в столовой?", variants:
                [PollVariant(text: "Да, устраивает"),
                 PollVariant(text: "Нет, не устраивает"),
                 PollVariant(text: "Где промокоды?"),
                 PollVariant(text: "Я таракана нашел в супе"),
                 PollVariant(text: "МТУСИ ван лав"),
                 PollVariant(text: "А я томат"),
                 PollVariant(text: "Бургер Кинг лучше")
                ], countToSelect: 2),
            PollQuestion(text: "На сколько нам нужно снизить цены?", variants: [
                PollVariant(text: "Всё и так окей"),
                PollVariant(text: "На 10%"),
                PollVariant(text: "На 30%"),
                PollVariant(text: "На 50%")])
            ], author: "Комбинат питания", date: "10.06.2017"),
        PollData(name: "Численные методы", description: "Agile, Big Data, Machine Learning!", questions: [
            PollQuestion(text: "Интересен ли вам факультативный семинар по численным методам в машинном обучении?", variants: [
                PollVariant(text: "Да, очень"),
                PollVariant(text: "Нет, не интересен"),
                PollVariant(text: "Затрудняюсь ответить")])
            ], author: "Кафедра ПМ АВТИ", date: "9.06.2017")]
    
    
    var actuality = [0,1,0]
    // MARK: - TableView Preparation
    func prepareTableView(){
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 500
    }
    
    // MARK: - View did load

    override func viewDidLoad() {
        super.viewDidLoad()
        prepareTableView()
    }
    
    // MARK: - Table view

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return polls.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PollTableViewCell", for: indexPath) as! PollTableViewCell
        cell.poll = polls[indexPath.row]
        cell.pollTextLabel.text = cell.poll!.getName()
        cell.pollAuthorLabel.text = cell.poll!.getAuthor()
        cell.pollDate.text = cell.poll!.getDate()
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! PollTableViewCell
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.performSegue(withIdentifier: "pollSelected", sender: cell)
    }
    
    // MARK: - Prepare for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            switch segue.identifier! {
            case "pollSelected":
                let dest = segue.destination as! PollPreviewViewController
                let sender = sender as! PollTableViewCell
                dest.poll = sender.poll!
            default: NSLog("Segue is not implemented")
            }
    }
    
    // MARK: - Mail sending

    @IBAction func addPollPressed(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            let mailVC = MFMailComposeViewController()
            mailVC.mailComposeDelegate = self
            mailVC.setSubject("Опрос в приложении \"НИУ МЭИ\"")
            mailVC.setToRecipients(["idisin91@gmail.com"])
            mailVC.setMessageBody("Оставьте данные для добавления собственного опроса, с вами свяжется администрация:\nИмя:\nОрганизация:\nКонтактные данные:\nТематика опроса:", isHTML: false)
            mailVC.mailComposeDelegate = self
            self.present(mailVC, animated: true, completion: nil)
        } else {
            NSLog("This device is unable to send mail!")
        }
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case MFMailComposeResult.cancelled:
            NSLog("Mail cancelled")
        case MFMailComposeResult.saved:
            NSLog("Mail saved")
        case MFMailComposeResult.sent:
            NSLog("Mail sent")
        case MFMailComposeResult.failed:
            NSLog("Mail sent failure: %@", [error?.localizedDescription])
        }
        self.dismiss(animated: true, completion: nil)
    }

}






