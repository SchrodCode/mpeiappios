//
//  TimetableViewController.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 07.06.17.
//  Copyright © 2017 ForMe. All rights reserved.
//
import UIKit
import JavaScriptCore

class TimetableViewController: UIViewController, UITableViewDataSource,UITableViewDelegate, StandartObserver, TimetableObserver {
    
    @IBOutlet weak var weekLabel: FancyLabel!
    @IBOutlet weak var navigation: UINavigationItem!
    @IBOutlet weak var bigTable: UITableView!
    @IBOutlet weak var switchButton: UIButton!
    var groupName: String?
    var selectedDay: Int = 0
    
    @IBAction func switchTimetable(_ sender: Any) {
        TimetableParser.instance.switchTimetable()
    }
    
    func prepareGroupData(){
        self.weekLabel.text = "7"
        self.weekLabel.backgroundColor = UIColor.MpeiApp.darkRedColor
        bigTable.estimatedRowHeight = 44
        bigTable.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserData.instance.addObserver(observer: self)
        TimetableParser.instance.addObserver(observer: self)
        prepareGroupData()
        navigationController?.hidesBarsOnSwipe = true
        System.makeShadow(view: headerView)
    }
    
    @IBAction func segmentedPressed(_ sender: Any) {
        let segmentedControl = sender as! UISegmentedControl
        let day = segmentedControl.selectedSegmentIndex
        selectedDay = day
        print("Selected day: \(selectedDay)")
        bigTable.reloadData()
    }
    
    @IBOutlet weak var headerView: UIView!
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TimetableParser.instance.length(i: selectedDay)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TimetableViewCell
        let timetable = TimetableParser.instance.getTimetable()
        let data = timetable[selectedDay][indexPath.row]
        cell.data = data
        cell.timeLabel.text = data.time
        cell.buildingButton.setTitle(data.building, for: .normal)
        cell.subjectLabel.text = data.subject
        cell.countLabel.text = data.count
        cell.teacherLabel.text = data.teacher
        cell.typeLabel.backgroundColor = #colorLiteral(red: 0.926155746, green: 0.9410773516, blue: 0.9455420375, alpha: 1)
        switch data.type {
        case .Exam:
            cell.typeLabel.text = "Экзамен"
        case .Lab:
            cell.typeLabel.text = "Лабораторные"
        case .Lecture:
            cell.typeLabel.text = "Лекция"
        case .Practice:
            cell.typeLabel.text = "Практика"
        case .Consult:
            cell.typeLabel.text = "Консультация"
        default:
            cell.typeLabel.text = "Другое"
        }
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let seg = segue.identifier {
        switch seg {
        case "session":
            let session = segue.destination as! SessionViewController
            session.groupName = self.groupName
        default: break
            }
        }
    }
    
    func groupNameChanged(data: String?) {
        self.groupName = data
        if self.groupName == nil {
            self.navigation.title = "Введите группу"
        } else {
            self.navigation.title = "Ваша группа: " + data!
        }
    }
    
    func userTypeChanged(data: Int?){}
    
    func timetableChanged(data: [[TimetableSubjectData]]){
        bigTable.reloadData()
    }
}
