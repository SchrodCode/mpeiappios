//
//  TableViewCell.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 08.06.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import UIKit
import Spring

@IBDesignable class TimetableViewCell: UITableViewCell {

    @IBOutlet weak var buildingButton: DesignableButton!
    
    @IBOutlet weak var subjectLabel: FancyLabel!
    @IBOutlet weak var timeLabel: FancyLabel!
    @IBOutlet weak var typeLabel: FancyLabel!
    @IBOutlet weak var countLabel: FancyLabel!
    @IBOutlet weak var teacherLabel: FancyLabel!
    
    var data: TimetableSubjectData?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: true)
    }

}
