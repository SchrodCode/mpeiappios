//
//  UserNavigationController.swift
//  MpeiApp
//


import UIKit

class UserNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.barTintColor = UIColor.MpeiApp.barTint
    }   
}
