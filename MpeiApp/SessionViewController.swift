//
//  SessionViewController.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 14.06.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import UIKit

class SessionViewController: UIViewController, UITableViewDataSource {
    var groupName: String?
    
    var timetable : [TimetableSubjectData] = [
        TimetableSubjectData(building: "М-909",subject: "Математический анализ",time: "13 июня 9:20", count: "К", type: TimetableSubjectType.Consult, teacher: "Черепова Марина Федоровна"),
        TimetableSubjectData(building: "М-909",subject: "Математический анализ",time: "14 июня 9:20", count: "1", type: TimetableSubjectType.Exam, teacher: "Черепова Марина Федоровна"),
        TimetableSubjectData(building: "М-708",subject: "Дифференциальные уравнения",time: "17 июня 11:10", count: "К", type: TimetableSubjectType.Consult, teacher: "Черепова Марина Федоровна"),
        TimetableSubjectData(building: "М-708",subject: "Дифференциальные уравнения",time: "19 июня 11:10", count: "2", type: TimetableSubjectType.Exam, teacher: "Черепова Марина Федоровна"),
        TimetableSubjectData(building: "А-302",subject: "Языки и методы программирования",time: "23 июня 13:45", count: "К",  type: TimetableSubjectType.Consult, teacher: "Черепова Марина Федоровна"),
        TimetableSubjectData(building: "А-302",subject: "Языки и методы программирования",time: "24 июня 13:45", count: "3",  type: TimetableSubjectType.Exam, teacher: "Черепова Марина Федоровна"),
        TimetableSubjectData(building: "Ж-206",subject: "Математическая логика",time: "29 июня 15:35", count: "К", type: TimetableSubjectType.Consult, teacher: "Черепова Марина Федоровна"),
        TimetableSubjectData(building: "Ж-206",subject: "Математическая логика",time: "30 июня 15:35", count: "4", type: TimetableSubjectType.Exam, teacher: "Черепова Марина Федоровна"),
        ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if groupName == nil {
            self.title = "Введите группу"

        } else {
            self.title = "Сессия: " + groupName!
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timetable.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TimetableViewCell
        let data = timetable[indexPath.row]
        cell.data = data
        cell.timeLabel.text = data.time
        cell.buildingButton.setTitle(data.building, for: .normal)
        cell.subjectLabel.text = data.subject
        cell.countLabel.text = data.count
        switch data.type {
        case .Exam:
            cell.typeLabel.text = "Экзамен"
            cell.typeLabel.backgroundColor = #colorLiteral(red: 0.920953393, green: 0.447560966, blue: 0.4741248488, alpha: 1)
        case .Lab:
            cell.typeLabel.text = "Лабораторные"
            cell.typeLabel.backgroundColor = #colorLiteral(red: 0.1919409633, green: 0.4961107969, blue: 0.745100379, alpha: 1)
        case .Lecture:
            cell.typeLabel.text = "Лекция"
            cell.typeLabel.backgroundColor = #colorLiteral(red: 0.8879843354, green: 0.5014117956, blue: 0, alpha: 1)
        case .Practice:
            cell.typeLabel.text = "Практика"
            cell.typeLabel.backgroundColor = #colorLiteral(red: 0.171055764, green: 0.6274157763, blue: 0.5123413205, alpha: 1)
        case .Consult:
            cell.typeLabel.text = "Консультация"
            cell.typeLabel.backgroundColor = #colorLiteral(red: 0.8322438002, green: 0.7636896968, blue: 0.5605030656, alpha: 1)
        default:
            cell.typeLabel.text = "Другое"
            cell.typeLabel.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
            
        }
        cell.typeLabel.frame.size.width = cell.typeLabel.frame.size.width + 20.0
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "subject":
            let subject = segue.destination as! SubjectViewController
            let sender = sender as! TimetableViewCell
            subject.data = sender.data!
        case "": break
        default: break
        }
    }

    
}
