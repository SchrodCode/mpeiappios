//
//  PollData.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 06.07.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import Foundation

enum QuestionType {
    case OneAnswer
    case ManyAnswers
}

class PollQuestion{
    //MARK: - Variables
    fileprivate var text : String?
    fileprivate var description: String?
    fileprivate var variants : [PollVariant]?
    fileprivate var photoURL : String?
    fileprivate var countToSelect: Int
    fileprivate var type: QuestionType
    var selectedVariantsCount : Int {
        var count : Int = 0
        for item in self.variants! {
            if item.isSelected() {
                count += 1
            }
        }
        return count
    }
    
    init(text: String, variants: [PollVariant], photoURL: String? = nil,  countToSelect: Int = 1, description: String = ""){
        self.text = text
        self.variants = variants
        self.photoURL = photoURL
        self.countToSelect = countToSelect
        if self.countToSelect == 1 {
            self.type = .OneAnswer
        } else {
            self.type = .ManyAnswers
        }
    }
    
    //MARK: - Variant
    func toSelect() -> Int {
        return countToSelect
    }
    
    func addObserver(observer: VariantObserver, atIndex: Int){
        if isValid(index: atIndex){
            self.variants?[atIndex].addObserver(observer: observer)
        }
    }
    
    func switchSelection(atIndex: Int){
        if isValid(index: atIndex){
            if (self.variants?[atIndex].isSelected())! {
                self.deselect(atIndex: atIndex)
            } else {
                self.select(atIndex: atIndex)
            }
        }
    }
    
    func select(atIndex: Int){
        if ((selectedVariantsCount < countToSelect) && isValid(index: atIndex)){
            self.variants?[atIndex].select()
        }
    }
    
    func deselect(atIndex: Int){
        if isValid(index: atIndex){
            self.variants?[atIndex].deselect()
        }
    }
    
    func getVariantText(atIndex: Int) -> String? {
        if isValid(index: atIndex){
            let text = self.variants?[atIndex].getText()
            return text
        }
        return "Error at index \(atIndex)"
    }
    
    func isVariantSelected(atIndex: Int) -> Bool {
        if isValid(index: atIndex){
            return self.variants![atIndex].isSelected()
        }
        return false
    }
    
    //MARK: - Getters
    func getType() -> QuestionType {
        return self.type
    }
    
    func getText() -> String? {
        return self.text
    }
    
    func getDescription() -> String? {
        return self.description
    }
    
    func getVariantsCount() -> Int? {
        return self.variants?.count
    }
    
    func show(){
        print(" text: \(text!)")
        for (i,variant) in (self.variants?.enumerated())! {
            print("  \(i) var: " + String((variant.isSelected())) + "\n")
        }
    }
    
    //MARK: - Validation
    fileprivate func isValid(index: Int) -> Bool {
        return ((index >= 0) && (index < (self.variants?.count)!))
    }
 
}




