//
//  PollPreviewViewController.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 07.07.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import UIKit

class PollPreviewViewController: UIViewController {

    @IBOutlet weak var pollName: UILabel!
    
    @IBOutlet weak var pollImage: UIImageView!
    
    @IBOutlet weak var pollDescription: UILabel!
    
    var poll : PollData?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pollName.text = poll?.getName()
        self.pollDescription.text = poll?.getDescription()
        
    }

    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "beginPoll":
            let pollAction = segue.destination as! PollActionViewController
            pollAction.poll = self.poll
        default: break
        }
    }
    

}
