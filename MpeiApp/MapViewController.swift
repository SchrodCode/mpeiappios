//
//  MapView.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 10.06.17.
//  Copyright © 2017 ForMe. All rights reserved.
//
import MapKit
import Foundation
import GoogleMaps

var markerimages: [UIImage] = [#imageLiteral(resourceName: "А"),#imageLiteral(resourceName: "Б"),#imageLiteral(resourceName: "В"),#imageLiteral(resourceName: "Г"),#imageLiteral(resourceName: "Д"),#imageLiteral(resourceName: "Е"),#imageLiteral(resourceName: "Ж"),#imageLiteral(resourceName: "З"),#imageLiteral(resourceName: "И"),#imageLiteral(resourceName: "К"),#imageLiteral(resourceName: "М"),#imageLiteral(resourceName: "Н")]
var markergeo: [[Double]] = [[55.756293, 37.708003],
                             [55.755399, 37.706952],
                             [55.755019, 37.707871],
                             [55.754663, 37.709276],
                             [55.755436, 37.710470],
                             [55.756665, 37.703072],
                             [55.754398, 37.707434],
                             [55.754344, 37.706565],
                             [55.753933, 37.708185],
                             [55.753728, 37.709430],
                             [55.756357, 37.704059],
                             [55.756988, 37.704145]]

class MapViewController : UIViewController {
    private var mapView: GMSMapView!

    @IBOutlet weak var navigation: UINavigationItem!
    

    override func loadView() {
        let camera = GMSCameraPosition.camera(withLatitude: 55.754964, longitude: 37.708110, zoom: 15)
        let map = GMSMapView.map(withFrame: .zero, camera: camera)
        map.accessibilityElementsHidden = false
        for i in 0..<markerimages.count {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: markergeo[i][0], longitude: markergeo[i][1])
            marker.icon = markerimages[i]
            marker.map = mapView
            marker.isFlat = false
        }
        map.setMinZoom(5, maxZoom: 17)
        self.view = map
    }
    
        override func viewDidLoad() {
        super.viewDidLoad()

    }
}
