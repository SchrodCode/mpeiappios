//
//  FancyLabel.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 07.06.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import Foundation
import UIKit
@IBDesignable class FancyLabel : UILabel {
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 { didSet {
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = cornerRadius > 0 }
    }
    @IBInspectable var borderWidth: CGFloat = 0.0 { didSet {
        layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor = .black { didSet {
        layer.borderColor = borderColor.cgColor }
    }
    
}
