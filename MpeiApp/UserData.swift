//
//  UserData.swift
//  MpeiApp
//

enum userType: Int {
    case Notype = 0
    case Student = 1
    case Teacher = 2
    case Abiturient = 3
}

import Foundation
class UserData: StandartSubject {
    private var groupName: String?
    private var userType: userType?
    var observerCollection = NSMutableSet()
    
    //MARK: Getters and setters
    func getUserTypeString()->String {
        if self.userType == nil {
            return "Тип не определён"
        }
        switch self.userType! {
            case .Student: return "Студент"
            case .Abiturient: return "Абитуриент"
            case .Teacher: return "Преподаватель"
            case .Notype: return "Не определён"
        }
    }
    
    func getUserTypeInt()->Int{
        guard self.userType != nil else {
            return 0
        }
        return (self.userType!.rawValue)
    }

    
    func getUserType()->userType?{
        return self.userType
    }
    

    func setUserType(newData: Int) {
        switch newData {
        case 0: self.userType = .Notype
        case 1: self.userType = .Student
        case 2: self.userType = .Teacher
        case 3: self.userType = .Abiturient
        default:
            break
        }
        UserDefaults.standard.set(newData, forKey: "userType")
        UserDefaults.standard.synchronize()
    }

    //Запросить данные конкретного наблюдателя
    func updateData(observer: StandartObserver){
        observer.groupNameChanged(data: self.groupName)
        observer.userTypeChanged(data: self.userType?.rawValue)
    }
    
    //MARK: Observer
    func addObserver(observer: StandartObserver) {
        self.observerCollection.add(observer)
        self.notifyObjects()
    }
    
    
    func removeObserver(observer: StandartObserver) {
        self.observerCollection.remove(observer)
    }
    
    
    func notifyObjects() {
        for observer in observerCollection {
            (observer as! StandartObserver).groupNameChanged(data: self.groupName)
            (observer as! StandartObserver).userTypeChanged(data: self.userType?.rawValue)

        }
    }
   
    
    func isEmpty() -> Bool{
        let bool = (self.userType == nil)
        return bool
    }
    
    

    func changeGroupName(newData: String?) {
        if newData == nil {
            UserDefaults.standard.removeObject(forKey: "groupName")
            UserDefaults.standard.synchronize()
        } else {
        UserDefaults.standard.set(newData, forKey: "groupName")
        UserDefaults.standard.synchronize()
        self.groupName = newData
        self.notifyObjects()
        }
    }
    
    func changeUserType(newData: Int?) {
        if newData == nil {
            UserDefaults.standard.removeObject(forKey: "userType")
            UserDefaults.standard.synchronize()
        } else {
        UserDefaults.standard.set(newData, forKey: "userType")
        UserDefaults.standard.synchronize()
        self.setUserType(newData: newData!)
        self.notifyObjects()
        }
    }

    //MARK: Singleton
    static let instance = UserData()
    
    private init(){
        if (UserDefaults.standard.object(forKey: "groupName") != nil){
            self.groupName = UserDefaults.standard.string(forKey: "groupName")
        }
        if (UserDefaults.standard.integer(forKey: "userType") != 0){
            let type = UserDefaults.standard.integer(forKey: "userType")
            self.changeUserType(newData: type)
        }
        self.notifyObjects()
        
    }
    
}
//MARK: - Protocols
protocol StandartObserver: class {
    func groupNameChanged(data: String?)
    func userTypeChanged(data: Int?)
}

protocol StandartSubject: class {
    func addObserver(observer: StandartObserver)
    func removeObserver(observer: StandartObserver)
    func notifyObjects()
}
