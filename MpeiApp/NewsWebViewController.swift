//
//  NewsWebViewController.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 20.06.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import UIKit
import Alamofire

class NewsWebViewController: UIViewController {
    
    @IBOutlet weak var navigation: UINavigationItem!
    @IBOutlet weak var webView: UIWebView!
    
    var data : NewsWebsiteData?
    
    @IBAction func openInSafari(_ sender: Any) {
        let url = (data?.newsUrl)!
        UIApplication.shared.open(url, options: [:],completionHandler: nil)
    }
    
    func loadNews(){
        Alamofire.request((data?.newsUrl)!).responseData { responseData in
            switch responseData.result {
            case .success(let value):
                guard let string = String(data: value, encoding: String.Encoding.utf8) else { return }
                var text = string.components(separatedBy: "<div id=\"onetIDListForm\">")[1]
                text = text.components(separatedBy: "<div class=\"rightBlockNews\">")[0]
                text = "<head><style type=\"text/css\">body{margin: 3%;visibility: hidden;}#breadcrumb{visibility: hidden;}.ms-rteElement-H1{text-align: center;font-family: \'Helvetica Neue\';font-size: 300%;visibility: visible;}.ms-rteImage-one{ align-self: middle;visibility: visible;border-width: 0.5px;border-style: outset;border-color: #222e40;margin: 3%;}.ms-rteElement-P{font-family: \'Helvetica Neue\';text-align: left;font-size: 230%;visibility: visible;padding-top: : 1.5%;padding-bottom: 1.5%;}.ms-rteTableOddRow-9{visibility: visible;}</style></head><body>" + text + "</body>"
                self.webView.loadHTMLString(text, baseURL: self.data?.newsUrl)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigation.title = "Новость от " + (data?.dateString)!
        self.loadNews()
        webView.scalesPageToFit = true
    }
}
