//
//  ChangeGroupViewController.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 22.06.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import UIKit

class ChangeGroupViewController: UIViewController, StandartObserver {
    
    @IBOutlet weak var textField: UITextField!
    
    func groupNameChanged(data: String?) {
        if data == nil {
            self.textField.placeholder = "Введите группу"
        } else {
            self.textField.placeholder = data!
        }
    }
    
    @IBAction func savePressed(_ sender: Any) {
        changeGroupFinish()
    }
    func changeGroupFinish(){
        if let newGroup = textField.text {
            if newGroup != "" {
            UserData.instance.changeGroupName(newData: newGroup)
            }
        }
        textField.endEditing(true)
        navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.changeGroupFinish()
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserData.instance.addObserver(observer: self)
        UserData.instance.notifyObjects()
        self.textField.keyboardAppearance = .light
        self.textField.becomeFirstResponder()
    }
    func userTypeChanged(data: Int?){
    }
}
