//
//  ChangeProfileViewController.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 28.06.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import UIKit

class ChangeProfileViewController: UIViewController, StandartObserver {
    @IBOutlet weak var abiturientCheck: FancyButton!
    @IBOutlet weak var studentCheck: FancyButton!
    @IBOutlet weak var teacherCheck: FancyButton!
    @IBAction func studentPressed(_ sender: Any) {
        UserData.instance.setUserType(newData: 1)
        System.noticeTop(sender: self, text: "Включен профиль студента")
        UserData.instance.updateData(observer: self)
    }
    
    @IBAction func teacherPressed(_ sender: Any) {
        UserData.instance.setUserType(newData: 2)
        System.noticeTop(sender: self, text: "Включен профиль преподавателя")
        UserData.instance.updateData(observer: self)
    }
    
    @IBAction func abiturientPressed(_ sender: Any) {
        UserData.instance.setUserType(newData: 3)
        System.noticeTop(sender: self, text: "Включен профиль абитуриента")
        UserData.instance.updateData(observer: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserData.instance.addObserver(observer: self)
        UserData.instance.updateData(observer: self)
    }
    
    func userTypeChanged(data: Int?){
        guard data != nil else {return}
        switch data! {
        case 1:
            studentCheck.isHidden = false
            teacherCheck.isHidden = true
            abiturientCheck.isHidden = true
        case 2:
            studentCheck.isHidden = true
            teacherCheck.isHidden = false
            abiturientCheck.isHidden = true
        case 3:
            studentCheck.isHidden = true
            teacherCheck.isHidden = true
            abiturientCheck.isHidden = false

        default:
            break
        }
    }
    
    func groupNameChanged(data: String?){
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "logout" {
            UserData.instance.setUserType(newData: 0)
            print("~~~~~ПЕРЕШЛИ НА ЭКРАН ЛОГИНА~~~~~")
        }
    }


}
