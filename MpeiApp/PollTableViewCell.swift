//
//  PollTableViewCell.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 06.07.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import UIKit

class PollTableViewCell: UITableViewCell {

    @IBOutlet weak var pollTextLabel: FancyLabel!
    
    @IBOutlet weak var pollDate: UILabel!
    
    @IBOutlet weak var pollState: UIView!
    
    @IBOutlet weak var pollAuthorLabel: UILabel!
    
    var poll : PollData?
    
}
