//
//  PollQuestionTableViewCell.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 07.07.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import UIKit

class PollQuestionTableViewCell: UITableViewCell, VariantObserver {
    
    @IBOutlet weak var questionTextLabel: UILabel!

    func statusChanged(checked: Bool) {
        if checked {
            self.accessoryType = .checkmark
        } else {
            self.accessoryType = .none
        }
    }
    
}
