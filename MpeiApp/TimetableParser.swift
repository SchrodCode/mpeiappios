//
//  TimetableParser.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 08.06.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import Foundation
import Alamofire

class TimetableParser: TimetableSubject {
    //MARK: singleton
    
    static let instance = TimetableParser()
    private init(){}
    
    
    private var isOdd = true
    fileprivate var observerCollection: NSMutableSet = []
    
    private var timetableOdd : [[TimetableSubjectData]] = [
        
        [   // Пн
        TimetableSubjectData(building: "М-708",subject: "Системное программное обеспечение",time: "9:20 - 10:55", count: "1", type: TimetableSubjectType.Practice, teacher: "Меньшикова К. Г."),
        
        TimetableSubjectData(building: "М-509",subject: "Системное программирование",time: "11:10 - 12:45", count: "2", type: TimetableSubjectType.Lecture, teacher: "Меньшикова К. Г."),
        
        TimetableSubjectData(building: "М-706",subject: "Программная инженерия",time: "13:45 - 15:20", count: "3", type: TimetableSubjectType.Practice, teacher: "Маран Микхель Микхелевич"),
        
        ],[ // Вт
            TimetableSubjectData(building: "М-706",subject: "Математическая логика",time: "11:10 - 12:45", count: "2", type: TimetableSubjectType.Practice, teacher: "Моросин О. Л."),
            
            TimetableSubjectData(building: "М-509",subject: "Математическая логика",time: "13:45 - 15:20", count: "3", type: TimetableSubjectType.Lecture, teacher: "Вагин В. Н."),
        
            TimetableSubjectData(building: "М-509",subject: "Математическая логика",time: "15:35 - 17:10", count: "4", type: TimetableSubjectType.Lecture, teacher: "Вагин В. Н.")
            
        ],[   // Ср
            TimetableSubjectData(building: "М-704",subject: "Архитектура ВС",time: "11:10 - 12:45", count: "2", type: TimetableSubjectType.Practice, teacher: "Шамаева О. Ю."),
            TimetableSubjectData(building: "М-807",subject: "Численные методы",time: "13:45 - 15:20", count: "3", type: TimetableSubjectType.Practice, teacher: "Казенкин К. О."),
            TimetableSubjectData(building: "М-708",subject: "Численные методы",time: "15:35 - 17:10", count: "4", type: TimetableSubjectType.Practice, teacher: "Казенкин К. О."),
            
        ],[ // Чт
            TimetableSubjectData(building: "М-508",subject: "Программная инженерия",time: "11:10 - 12:45", count: "2", type: TimetableSubjectType.Practice, teacher: "Маран М. М."),
            TimetableSubjectData(building: "Г-404",subject: "Численные методы",time: "13:45 - 15:20", count: "3", type: TimetableSubjectType.Lecture, teacher: "Амосова О. А."),
            TimetableSubjectData(building: "М-706",subject: "Компьютерная графика",time: "15:35 - 17:10", count: "4", type: TimetableSubjectType.Lecture, teacher: "Бартеньев О. В.")
            
        ],[ // Пт
            TimetableSubjectData(building: "М-706",subject: "Архитектура ВС (лаба)",time: "9:20 - 10:55", count: "1", type: TimetableSubjectType.Lecture, teacher: ""),
            TimetableSubjectData(building: "М-706",subject: "Архитектура ВС (лаба)",time: "11:10 - 12:45", count: "2", type: TimetableSubjectType.Lecture, teacher: ""),
            TimetableSubjectData(building: "",subject: "Физкультура",time: "13:45 - 15:20", count: "3", type: TimetableSubjectType.Other, teacher: ""),
            
        ],[ // Cб
            TimetableSubjectData(building: "",subject: "День самостоятельных занятий",time: "00:00 - 23:59", count: "0", type: TimetableSubjectType.Other, teacher: ""),
            ]
    ]
    
    private var timetableEven : [[TimetableSubjectData]] = [
        [
            // Пн
            TimetableSubjectData(building: "М-509",subject: "Системное программное обеспечение",time: "9:20 - 10:55", count: "1", type: TimetableSubjectType.Practice, teacher: "Меньшикова К. Г."),
            
            TimetableSubjectData(building: "М-509",subject: "Системное программирование",time: "11:10 - 12:45", count: "2", type: TimetableSubjectType.Lecture, teacher: "Меньшикова К. Г."),
            
            TimetableSubjectData(building: "М-706",subject: "Программная инженерия",time: "13:45 - 15:20", count: "3", type: TimetableSubjectType.Practice, teacher: "Маран Микхель Микхелевич"),
            
            ],
        
        [   // Вт
            TimetableSubjectData(building: "М-706",subject: "Математическая логика",time: "11:10 - 12:45", count: "2", type: TimetableSubjectType.Practice, teacher: "Моросин О. Л."),
            
            TimetableSubjectData(building: "М-509",subject: "Математическая логика",time: "13:45 - 15:20", count: "3", type: TimetableSubjectType.Lecture, teacher: "Вагин В. Н.")],
        
        [   // Ср
            TimetableSubjectData(building: "М-809",subject: "Архитектура ВС",time: "9:20 - 10:55", count: "1", type: TimetableSubjectType.Practice, teacher: "Шамаева О. Ю."),
            
            TimetableSubjectData(building: "Г-308",subject: "Компьютерная графика",time: "11:10 - 12:45", count: "2", type: TimetableSubjectType.Practice, teacher: "Бартеньев О. В."),
            
            TimetableSubjectData(building: "Кафедра ММ",subject: "Численные методы",time: "13:45 - 15:20", count: "3", type: TimetableSubjectType.Consult, teacher: ""),
            
            TimetableSubjectData(building: "",subject: "Физкультура",time: "15:35 - 17:10", count: "4", type: TimetableSubjectType.Practice, teacher: ""),
            ],
        // Чт
        [
            TimetableSubjectData(building: "М-508",subject: "Программная инженерия",time: "11:10 - 12:45", count: "2", type: TimetableSubjectType.Practice, teacher: "Маран М. М."),
            
            TimetableSubjectData(building: "Г-404",subject: "Численные методы",time: "13:45 - 15:20", count: "3", type: TimetableSubjectType.Lecture, teacher: "Амосова О. А."),
            
            TimetableSubjectData(building: "М-706",subject: "Компьютерная графика",time: "15:35 - 17:10", count: "4", type: TimetableSubjectType.Lecture, teacher: "Бартеньев О. В.")],
        // Пт
        [
            TimetableSubjectData(building: "Кафедра ПМ",subject: "Программная инженерия",time: "11:10 - 12:45", count: "2", type: TimetableSubjectType.Consult, teacher: ""),
            TimetableSubjectData(building: "М-708",subject: "Физкультура",time: "13:45 - 15:20", count: "3", type: TimetableSubjectType.Other, teacher: ""),
            ],
        // Cб
        [
            TimetableSubjectData(building: "",subject: "День самостоятельных занятий",time: "00:00 - 23:59", count: "0", type: TimetableSubjectType.Other, teacher: ""),
            ]
    ]
    
    func switchTimetable(){
        isOdd = !isOdd
        notifyObjects()
    }
    func odd() -> Bool {
        return isOdd
    }
    
    func getTimetable() -> [[TimetableSubjectData]] {
        if isOdd {
            return timetableOdd
        } else {
            return timetableEven
        }
    }
    
    func length(i: Int) -> Int {
        let timetable = getTimetable()
        return timetable[i].count
    }
    
    
}

// MARK: observer
extension TimetableParser {
    
    func addObserver(observer: TimetableObserver){
        observerCollection.add(observer)
        notifyObjects()
    }
    
    func removeObserver(observer: TimetableObserver){
        observerCollection.remove(observer)
    }
    
    func notifyObjects() {
        for item in observerCollection {
            (item as! TimetableObserver).timetableChanged(data: getTimetable())
        }
    }
}

protocol TimetableObserver {
    func timetableChanged(data: [[TimetableSubjectData]])
}

protocol TimetableSubject{
    func addObserver(observer: TimetableObserver)
    func removeObserver(observer: TimetableObserver)
    func notifyObjects()
}


