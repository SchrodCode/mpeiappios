//
//  NewsTableViewCell.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 16.06.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import UIKit
enum NewsCellType {
    case News
    case Advert
    case EmptyPlaceholder
}


class NewsTableCell: UITableViewCell {

}

class NewsTableViewCell: NewsTableCell {
    @IBOutlet weak var newsDate: FancyLabel!
    @IBOutlet weak var newsText: FancyLabel!
    @IBOutlet weak var sourseLabel: FancyLabel!
    @IBOutlet weak var sourceImage: UIImageView!
    var data: NewsWebsiteData?
    
    @IBOutlet weak var separatorView: UIView!
    override func layoutSubviews() {
        super.layoutSubviews()
        let border1 = CALayer()
        let width = CGFloat(0.5)
        border1.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        border1.frame = CGRect(x: 0, y: separatorView.frame.size.height - width, width:  separatorView.frame.size.width, height: separatorView.frame.size.height)
        border1.borderWidth = width
        separatorView.layer.addSublayer(border1)
        let border2 = CALayer()
        border2.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        border2.frame = CGRect(x: 0, y:width, width:  separatorView.frame.size.width, height: separatorView.frame.size.height)
        border2.borderWidth = width
        separatorView.layer.addSublayer(border2)
        separatorView.layer.masksToBounds = true

    }
}

class NewsAdvertViewCell: NewsTableCell {
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var adTitleLabel: UILabel!
    
    @IBOutlet weak var separatorView: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let border1 = CALayer()
        let width = CGFloat(0.5)
        border1.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        border1.frame = CGRect(x: 0, y: separatorView.frame.size.height - width, width:  separatorView.frame.size.width, height: separatorView.frame.size.height)
        border1.borderWidth = width
        separatorView.layer.addSublayer(border1)
        let border2 = CALayer()
        border2.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        border2.frame = CGRect(x: 0, y:width, width:  separatorView.frame.size.width, height: separatorView.frame.size.height)
        border2.borderWidth = width
        separatorView.layer.addSublayer(border2)
        separatorView.layer.masksToBounds = true
    }
}

class EmptyPlaceholderViewCell: NewsTableCell {

}





