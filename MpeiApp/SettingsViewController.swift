//
//  SettingsViewController.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 21.06.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import UIKit
import Spring
import MessageUI

class SettingsViewController: UIViewController, StandartObserver, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var profileLabel: UILabel!
    
    @IBOutlet weak var navigation: UINavigationItem!
    @IBOutlet weak var aboutBox: SpringView!
    @IBOutlet weak var feedbackBox: SpringView!
    @IBOutlet weak var groupBox: SpringView!
    
    
    @IBOutlet weak var bottomSpaceGroupConstraint: NSLayoutConstraint!
    
    var group: String?
    var user: Int?
    @IBAction func sendFeedback(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            let mailVC = MFMailComposeViewController()
            mailVC.mailComposeDelegate = self
            mailVC.setSubject("Отзыв о приложении \"НИУ МЭИ\"")
            mailVC.setToRecipients(["idisin91@gmail.com"])
            mailVC.setMessageBody("Версия приложения: 1.0\n Текущий профиль: \"\(UserData.instance.getUserTypeString())\"", isHTML: false)
            mailVC.mailComposeDelegate = self
            self.present(mailVC, animated: true, completion: nil)
        } else {
            NSLog("This device is unable to send mail!")
        }
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case MFMailComposeResult.cancelled:
            NSLog("Mail cancelled")
        case MFMailComposeResult.saved:
            NSLog("Mail saved")
        case MFMailComposeResult.sent:
            NSLog("Mail sent")
        case MFMailComposeResult.failed:
            NSLog("Mail sent failure: %@", [error?.localizedDescription])
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func languageChanged(_ sender: UISegmentedControl) {
        System.noticeTop(sender: self, text: "Смена языка временно недоступна")
    }
    
    @IBAction func aboutClicked(_ sender: Any) {
        
    }
    @IBOutlet weak var groupLabel: FancyLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserData.instance.addObserver(observer: self)
        UserData.instance.notifyObjects()
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.aboutClicked (_:)))
        aboutBox.addGestureRecognizer(gesture)
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(self.feedbackClicked(_:)))
        feedbackBox.addGestureRecognizer(gesture2)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
    }
    func feedbackClicked(_ sender: Any){
        
    }

    func groupNameChanged(data: String?) {
        self.group = data
        if data == nil {
            self.groupLabel.text = "Ошибка"
        } else {
            self.groupLabel.text = data!
        }
    }
    
    func userTypeChanged(data: Int?){
        self.user = data
        if self.user == 1 {
            self.groupBox.isHidden = false
        } else {
            self.groupBox.isHidden = true
        }
        self.profileLabel.text = UserData.instance.getUserTypeString()
        
    }


}
