//
//  NewsViewController.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 15.06.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import UIKit

import Alamofire


class NewsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    //MARK: Refresh Control variables
    var refreshControl:UIRefreshControl!
    var customView: UIView!
    var isAnimating = false
    var timer: Timer!
    var refreshLabel: UILabel?
    
    @IBOutlet weak var navigation: UINavigationItem!
    @IBOutlet weak var newsTable: UITableView!
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NewsParser.instance.cellsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return NewsCellFactory.getCell(at: indexPath, vc: self)
    }

    func refresh() {
        NewsParser.instance.refreshNews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //prepareTableView()
        prepareRefreshControl()
        prepareNewsObserver()
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let deltaOffset = maximumOffset - currentOffset
        if (deltaOffset <= 0) {
            loadMore()
        }
    }
    
    func loadMore() {
        NewsParser.instance.parseNews()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return NewsCellFactory.getHeightForCell(at: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "newsWeb":
            let web = segue.destination as! NewsWebViewController
            let sender = sender as! NewsTableViewCell
            web.data = sender.data
        default: break
        }
    }
}


// MARK: Refresh Animation
extension NewsViewController {
    func loadCustomRefreshContents() {
        let refreshContents = Bundle.main.loadNibNamed("RefreshContent", owner: self, options: nil)
        customView = refreshContents?[0] as! UIView
        customView.frame = refreshControl.bounds
        refreshLabel = customView.viewWithTag(1) as? UILabel
        refreshControl.addSubview(customView)
    }
    
    func animateRefresh() {
        UIView.animate(withDuration: 0.55, delay: 0.7, options: UIViewAnimationOptions.curveLinear, animations: { () -> Void in
            self.refreshLabel?.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        }, completion: { (finished) -> Void in
            UIView.animate(withDuration: 0.55, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations: { () -> Void in
                self.refreshLabel?.transform = CGAffineTransform.identity
            }, completion: { (finished) -> Void in
                if self.refreshControl.isRefreshing {
                    self.animateRefresh()
                }
                else {
                    self.isAnimating = false
                    self.refreshLabel?.transform = CGAffineTransform.identity
                }
            })}
        )
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if refreshControl.isRefreshing {
            if !isAnimating {
                startTimer()
                animateRefresh()
            }
        }
    }

    func prepareRefreshControl(){
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = UIColor.clear
        
        newsTable.addSubview(refreshControl)
        loadCustomRefreshContents()
    }
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(NewsViewController.endRefreshing), userInfo: nil, repeats: true)
    }
    
    func endRefreshing() {
        refreshControl.endRefreshing()
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
    }
}

//MARK: NewsObserver
extension NewsViewController: NewsObserver {
    func newsDataChanged(){
        newsTable.reloadData()
    }
    
    func prepareNewsObserver(){
        NewsParser.instance.addObserver(observer: self)
    }
}
