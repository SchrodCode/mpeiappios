//
//  LoginViewController.swift
//  MpeiApp
//
//  Created by Никита Скрынников on 27.06.17.
//  Copyright © 2017 ForMe. All rights reserved.
//

import UIKit
import Spring

class LoginViewController: UIViewController {

    @IBOutlet weak var chooseProfileLabel: DesignableLabel!
    @IBOutlet weak var studentButton: DesignableButton!
    @IBOutlet weak var teacherButton: DesignableButton!
    @IBOutlet weak var abuturientButton: DesignableButton!
    
    override func viewDidLoad() {
        print("ВНУТРИ ЛОГИНА")
        super.viewDidLoad()
        if !(UserData.instance.isEmpty()){
            performLogin()
        } else {
            showInterface()
        }
    }
    
    func performLogin(){
        let type = UserData.instance.getUserType()!
        switch type.rawValue {
        case 1: DispatchQueue.main.async(){
            self.performSegue(withIdentifier: "studentButton", sender: self)}
        case 2:
            DispatchQueue.main.async(){self.performSegue(withIdentifier: "teacherButton", sender: self)}
        case 3:
            DispatchQueue.main.async(){self.performSegue(withIdentifier: "abiturientButton", sender: self)}
        default:
            break
        }
    }
    
    //MARK: Interface
    func showInterface(){
        chooseProfileLabel.isHidden = false
        studentButton.isHidden = false
        teacherButton.isHidden = false
        abuturientButton.isHidden = false
    }
    
    func hideInterface(){
            chooseProfileLabel.isHidden = true
            studentButton.isHidden = true
            teacherButton.isHidden = true
            abuturientButton.isHidden = true
    }
    
    //MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "studentButton" {
            UserData.instance.setUserType(newData: 1)
        }
        if segue.identifier == "teacherButton" {
            UserData.instance.setUserType(newData: 2)
        }
        if segue.identifier == "abiturientButton" {
            UserData.instance.setUserType(newData: 3)
        }
    }
}
